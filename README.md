Meson and C build for simple OpenGL, GLEW, and GLFW.

Esc to exit.

![Screenshot of running program](screenshot.png)

# Dependencies

	apt install libglew-dev libglfw3-dev libgraphene-1.0-dev

# Build

	meson build
	ninja -C build
	ninja -C build scan-build # static analysis.

# References 

- <http://glew.sourceforge.net/> 
- <http://www.glfw.org/>
- <https://open.gl>
- <https://www3.ntu.edu.sg/home/ehchua/programming/opengl/CG_Introduction.html>

